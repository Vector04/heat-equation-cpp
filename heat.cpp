#include <cmath>
#include <iostream>
#include <initializer_list>
#include <memory>
#include <map>
#include <stdexcept>
#include <utility>
#include <type_traits>

using std::cout, std::endl;

template<class> class Vector;
template<class> class Matrix;
template<int, class> class Heat;

template <typename T>
class Vector
{
public:
    Vector()
    {
        length = 0;
        data = nullptr;
    }

    Vector(int size)
    {
        length = size;
        data = new T[size];
    }

    Vector(const Vector<T>& other)
    : Vector(other.length)
    {
        for (int i = 0; i < length; i++) {
            data[i] = other.data[i];
        }
    }

    Vector(Vector<T>&& other)
    {
        length = other.length;
        data = other.data;

        other.length = 0;
        other.data = nullptr;
    }

    Vector(std::initializer_list<T> list)
    : Vector(list.size())
    {
        std::uninitialized_copy(list.begin(), list.end(), data);
    }

    ~Vector()
    {
        length = 0;
        delete[] data;
        data = nullptr;
    }

    Vector<T>& operator=(Vector<T>& other) {
        if (length != other.length) {
            length = other.length;
            delete[] data;

            data = new T[length];
        }

        for (int i = 0; i < length; i++) {
            data[i] = other.data[i];
        }

        return *this;
    }
    Vector<T>& operator=(Vector<T>&& other) {
        delete[] data;

        length = other.length;
        data = other.data;

        other.data = nullptr;
        other.length = 0;

        return *this;
    }

    T& operator[](int i) {
        i %= length;
        return data[i];
    }

    const T& operator[](int i) const {
        i %= length;
        return data[i];
    }
    
    template<typename U>
    Vector<std::common_type_t<T, U>> operator+(const Vector<U>& other) const {
        if (length != other.len()) {
            throw std::length_error("Vector Lengths (" + std::to_string(length) + " and " + std::to_string(other.len()) + ") to not match");
        }
        Vector<std::common_type_t<T, U>> output(length);
        for (int i = 0; i < length; i++) {
            output[i] = data[i] + other[i];
        }
        return output;
    }

    template<typename U>
    Vector<std::common_type_t<T, U>> operator-(const Vector<U>& other) const {
        if (length != other.len()) {
            throw std::length_error("Vector Lengths (" + std::to_string(length) + " and " + std::to_string(other.len()) + ") to not match");
        }
        Vector<std::common_type_t<T, U>> output(length);
        for (int i = 0; i < length; i++) {
            output[i] = data[i] - other[i];
        }
        return output;
    }

    template<typename U>
    Vector<std::common_type_t<T, U>> operator*(U scalar) const {
        Vector<std::common_type_t<T, U>> output(length);
        for (int i = 0; i < length; i++) {
            output[i] = data[i] * scalar;
        }
        return output;
    }

    int len() const {
        return length;
    }

private:
    int length;
    T* data;
};

template<typename T>
std::ostream& operator<<(std::ostream &cout, const Vector<T> &v) { 
    cout << "[";
    int L = v.len();
    if (L < 40) {
        for (int i = 0; i < L - 1; i++) {
            cout << v[i] << ", ";
        }
        cout << v[L-1] << "]";
    } else {
        cout << v[0] << ", " << v[1] << ", " <<  v[2] << " ... " << v[L-3] << ", " << v[L-2] << ", " << v[L-1] << "]";
    }
    return cout;
}

template<typename U, typename T>
std::enable_if_t< std::is_arithmetic_v<U>, Vector< std::common_type_t<T, U> > > 
operator*(U scalar, const Vector<T>& v) {
    Vector<std::common_type_t<T, U>> output(v.len());
    for (int i = 0; i < v.len(); i ++) {
        output[i] = scalar * v[i];
    }
    return output;
}

template<typename T, typename U>
typename std::common_type_t<T,U> dot(const Vector<T>& lhs, const Vector<U>& rhs)
{
    if (lhs.len() != rhs.len()) {
        throw std::length_error("Vector Lengths ("+ std::to_string(lhs.len()) + " and " + std::to_string(rhs.len()) + ") to not match");
    }
    std::common_type_t<T, U> output = 0;
    for (int i = 0; i < lhs.len(); i++) {
        output = output + lhs[i] * rhs[i];
    }
    return output;
}

template <typename T>
class Matrix
{
public:
    Matrix(int rows, int columns)
    : rows(rows), columns(columns) {}

    ~Matrix() {}

    T& operator[](const std::pair<int, int>& ij) {
        return data[{ij.first % rows, ij.second % columns}];
    }

    const T& operator()(const std::pair<int, int>& ij) const {
        return data.at({ij.first % rows, ij.second % columns});
    }

    bool is_empty(const std::pair<int, int>& ij) const {
        return !data.count(ij);
    }

    template<typename U>
    Vector<std::common_type_t<T, U>> operator*(const Vector<U>& v) const { 
        // n x m multiplied by m x 1 vector
        if (columns != v.len()) {
            throw std::length_error("The matrix and vector are of incorrect size (" + std::to_string(rows) + ", " + std::to_string(columns) + ") and (" + std::to_string(v.len()) + ", 1)");
        }

        Vector<std::common_type_t<T, U>> output(rows);
        for (int i = 0; i < rows; i++) {
            output[i] = 0;
        }

        for (auto it = data.begin(); it != data.end(); ++it) {
            int i = it->first.first;
            int j = it->first.second;
            
            T value = it->second;
            output[i] += value * v[j];
        }
        return output;
    }

    int getRows() const {
        return rows;
    }

    int getColumns() const {
        return columns;
    }

private:
    int rows;
    int columns;
    std::map<std::pair<int, int>, T> data;
};

template<typename T>
std::ostream& operator<<(std::ostream &cout, const Matrix<T> &M) { 
    cout << "[";
    for (int i = 0; i < M.getRows(); i++) {
        cout << "[";
        for (int j = 0; j < M.getColumns(); j++) {
            std::string val;
            try {
                val = std::to_string(M({i, j})).substr(0, 4);
            } catch (std::out_of_range& e) {
                val = "    ";
            }
            cout << val << (j < M.getColumns() - 1 ? ", " : "");
        }
        cout << (i < M.getRows() - 1 ? "]\n " : "]]");
    }
    return cout;
}

template<typename T>
int cg(const Matrix<T>& A, 
       const Vector<T>& b, 
       Vector<T>&       x, 
       T                tol     = (T)1e-8, 
       int              maxiter = 100)
{
    Vector<T> p = b - (A * x);
    Vector<T> r = p;

    T alpha, beta;
    Vector<T> r_new(r.len());

    for (int k = 1; k <= maxiter; k++) {
        alpha = dot(r, r) / dot(A * p, p);

        x = x + (alpha * p);
        r_new = r - alpha * (A * p);
        
        if (dot(r_new, r_new) < (tol * tol)) {
            return k;
        }

        beta = dot(r_new, r_new) / dot(r, r);
        p = r_new + beta * p;

        r = r_new;
    }
    return -1;
}

template <int n, typename T>
class Heat
{
public:
    T alpha;
    int m;
    T dt;
    int d;
    Matrix<T> M;
    const T pi = std::atan(1) * 4;

    Heat(T alpha, int m, T dt) 
    : alpha(alpha), m(m), dt(dt), d(std::pow(m, n)), M(d, d) {

        T dx = T(1.0) / T(m+1);
        T prefactor = alpha * dt / (dx * dx);

        for (int i = 0; i < d; i++) {
            // M_ii, first part of diagonal term
            M[{i, i}] = 1;

            // Node j has neighbours j ± m**k, 0 <= k < n
            for (int k = 0; k < n; k++) {
                M[{i, i}] += 2 * prefactor;

                int dj = std::pow(m, k);
                
                if ((i + dj) < d) {
                    M[{i, i + dj}] = -prefactor;
                }
                if ((i - dj) >= 0) {
                     M[{i, i - dj}] = -prefactor;
                }
            }
        }
    }

    Vector<T> coordinates(int node) {
        Vector<T> coord(n);
        // x_0 = ((j mod m) + 1 ) / (m + 1)
        // x_1 = (((j // m) mod m) + 1 ) / (m + 1)
        // x_2 = (((j // m^2) mod m) + 1 ) / (m + 1)
        // The general formula: x_k = ((j / m**k) mod m + 1) / (m+1)
        for (int k = 0; k < n; k++) {
            coord[k] = T(((node / int(std::pow(m, k))) % m) + 1) / (m + 1);
        }
        return coord;
    }

    Vector<T> initial_condition() {
        Vector<T> u_0(d);
        for (int j = 0; j < d; j++) {
            Vector<T> coord = coordinates(j);
            u_0[j] = 1;
            for (int k = 0; k < n; k++) {
                u_0[j] *= std::sin(pi * coord[k]);
            }
        }
        return u_0;
    }

    Vector<T> exact(T t) {
        return std::exp(- n * pi * pi * alpha * t) * initial_condition();
    }

    Vector<T> solve(T t) {
        
        Vector<T> ic = initial_condition();   
        auto solution = 0 * ic;

        for (T s = 0; s < t; s += dt) {
            cg(M, ic, solution);
            ic = solution;
        }
        return solution;
    }
};

int main(int argc, char* argv[])
{
    Vector<int> v1;
    Vector<float> v2(3);
    Vector<double> v3 = {1, 2, 3};
    Vector<double> v4 = {1, 2, 3, 4};
    Vector<double> v5 = v3;

    // Some tests
    v2 + v3;
    v2 - v3;
    dot(v3, v5);
    v3[0] = 10;

    Matrix<double> M1(3,3);
    M1[{0, 0}] = 3;
    M1[{0, 0}];
    M1 * v3;

    Heat<1, double> heat1(0.3125, 3, 0.1); cout << heat1.M << endl << endl;
    Heat<2, double> heat2(0.3125, 3, 0.1); cout << heat2.M << endl << endl;
    Heat<2, double> heat(0.3125, 99, 0.001);

    Vector<double> sol1 = heat.solve(0.5);
    Vector<double> sol2 = heat.exact(0.5);
    auto diff = sol1 - sol2;
    auto MSE = dot(diff, diff) / heat.d;
    cout << "Mean squared error between the solutions: " << MSE << endl;

    cout << "From iteration:\n" << sol1 << endl;
    cout << "Exact solution:\n" << sol2 << endl;
    return 0;
}